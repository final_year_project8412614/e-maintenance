const AppError = require('../utils/appError');
const User = require('./../models/userModels')
const multer = require("multer")
const nodemailer = require("nodemailer");
const randomstring = require("randomstring");
const dotenv = require('dotenv')
dotenv.config({path:'./config.env'})
const bcrypt = require('bcryptjs')

var OTP = '602700' ;

const sendResetPasswordMail = async(name,email,token)=>{
    try {
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:process.env.emailUser,
                pass:process.env.emailPassword
            }
        });

        const mailOptions = {
            from:process.env.emailUser,
            to: email,
            subject:'For Reset password',
            html:`<p> Hii `+name+`, Please copy the token </p><br>
            <h1>`+token+`</h1><br> 
            <p>enter OTP to reset your password</p>` 
        }
        transporter.sendMail(mailOptions,function(error,info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Mail has been sent:- ",info.response);
            }
        })
        
    } catch (error) {
        res.status(400).send({success:false,msg:error.message})
    }
}


const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'views/img/users');
  },
  filename: (req, file, cb) => {
    var obj = JSON.parse(req.cookies.token)
    const ext = file.mimetype.split('/')[1];
    cb(null, `user-${obj['_id']}-${Date.now()}.${ext}`)
},
})
const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      cb(null, true);
    } else {
      cb(new AppError('Not an image! Please upload only images.', 400), false);
    }
};
const upload = multer({
    storage: multerStorage,
    fileFilter: multerFilter
});
exports.uploadUserPhoto = upload.single('photo');


const filterObj = (obj, ...allowedFields) => {
    const newObj = {}
    Object.keys(obj).forEach(el => {
      if (allowedFields.includes(el)) newObj[el] = obj[el]
    })
    return newObj
}
exports.updateMe = async (req, res, next) => {
    try{
        // 1) Create error if user POSTs password data
    if (req.body.password || req.body.passwordConfirm) {
        return next(
            new AppError(
            'This route is not for password updates. Please use /updateMyPassword.',
            400,
            ),
        )
    }
    // 2) Filtered out unwanted fields names that are not allowed to be updated
    const filteredBody = filterObj(req.body, 'name', 'email','employee_id')
    if (req.body.photo !== "undefined") {
        filteredBody.photo = req.file.filename
    }
    var obj = JSON.parse(req.cookies.token)
    const updatedUser = await User.findByIdAndUpdate(obj['_id'], filteredBody, {
        new: true,
        runValidators: true,
    })
    res.status(200).json({
        status: 'success',
        data: {
            user: updatedUser
        }
    })

   }
   catch (err){
       res.status(500).json({ error: err.message});
   }

}
exports.forget_password = async(req,res)=>{
    try {
        const email = req.body.email;
        const userData = await User.findOne({email:email});
        
        if(userData){
            const expirationTime = new Date();
            expirationTime.setMinutes(expirationTime.getMinutes() + 5);
            const randomString = randomstring.generate(6);
            OTP = randomString;
            // console.log(OTP)
            const data = await User.updateOne({email:email},{$set:{token:randomString, tokenExpiration: expirationTime}});
            sendResetPasswordMail(userData.name,userData.email,randomString); 
            res.status(200).json({
                status:'success',
                msg:"MailDone"})

        }
        else{
            res.status(200).json({
                status:'success',
                msg:"NoMail"})
        }

    } catch (error) {
        res.status(400).json({
            error:err.message});
    }
}
exports.enter_OTP = async(req,res)=>{
    try {
        const OTP_recieved = req.body.otp;
        if(OTP_recieved===OTP){
            res.status(200).json({status:'success',msg:"Correct OTP"});
        }
        else{
            res.status(200).json({error:'error',msg:"OPT invalid"});
        }
    } catch (error) {
        res.status(500).json({error:err.message});
    }

}


exports.reset_password = async(req,res)=>{
    try {
        const tokenData = await User.findOne({token:OTP});
        if(tokenData){
            const password = req.body.password;
            const newpassword = await bcrypt.hash(password,12);
            const userData = await User.findByIdAndUpdate({_id:tokenData._id},{$set:{password:newpassword,token:''}},{new:true})
            console.log("password successfully changed")
            res.status(200).json({status:'success',msg:"User password has been reset ", data:userData})
        }
        else{
            res.status(200).json({status:'error',msg:"This link has been expired."})
        }
    } catch (error) {
        res.status(400).json({msg:error.message});        
    }
}

exports.getAllUsers = async(req,res,next)=>{
    try{
        const users = await User.find()
        res.status(200).json({data:users, status:'success'});
    } catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.createUser = async(req,res)=>{
    try{
        const users = await User.create(req.body);
        console.log(req.body.name)
        res.json({data:users, status:'success'});
    } catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.getUser = async(req,res)=>{
    try{
        const users = await User.findById(req.params.id);
        res.json({data:users, status:'success'});
    } catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.updateUser = async(req,res)=>{
    try{
        const users = await User.findByIdAndUpdate(req.params.id ,req.body);
        res.json({data:users, status:'success'});
    } catch(err){
        res.status(500).json({error:err.message});
    }
}

exports.deleteUser = async(req,res)=>{
    try{
        const users = await User.findByIdAndDelete(req.params.id);
        res.json({data:users, status:'success'});
    } catch(err){
        res.status(500).json({error:err.message});
    }
}

