import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'

const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}

const yearlyTask = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks/yearly';
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks/yearly';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            const data = res.data;
            const Years = Object.keys(data.data.groupedPosts)
            // const Years = ["2001", "2002","2003","2004","2005","2006",]
            var text = ``
            if (Years){
                for (let year in Years) {
                    text += `<a href="/completeTask?id=${Years[year]}"><div class="card">
                            <div class="card-info">
                                <h4 class="card-title">Year</h4>
                                <h1 class="card-year">${Years[year]}</h1>
                            </div>
                            </div></a>`
                }
            }
            document.querySelector('.all-card').innerHTML = text
        }
    }catch(err){
        showAlert('error', 'Error getting years.')
    }
}

yearlyTask();


var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <a href = "/me"><h4>My profile</h4></a>
            <a id = "logout"><h4>Log out</h4></a>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}

const login = async (employee_id,password)=>{
    console.log(employee_id,password)
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/login';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/login';
    }
    try{
        const res = await axios({
            method : 'POST',
            url:URL,
            data:{
                employee_id,
                password,
            },
        })
        if(res.data.status === 'success'){
            showAlert('success','Logged in successfully')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
            var obj = res.data.data.user
            console.log(obj)
            document.cookie = 'token = '+JSON.stringify(obj)
            console.log(obj)
        }
    }catch(err){
        let message = 
            typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        showAlert('error','Error: Incorrect employeeid or password',message)
    }
};
document.querySelector('.login-form').addEventListener('submit',e=>{
    e.preventDefault();
    const employeeid = document.getElementById('employeeid').value;
    const password  = document.getElementById('password').value
    login(employeeid,password);
    // console.log(employeeid,password)
});
document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}








