const express =  require('express')
const router = express.Router()
const viewsController = require('../controllers/viewController')
const authController = require('../controllers/authController')
const userController = require('./../controllers/userController')


router.get('/',viewsController.getHome)
// router.get('/login',viewsController.getLoginForm)
router.get('/signup',viewsController.getSignupForm)
router.get('/me',authController.protect, viewsController.getprofile)
router.get('/forgetpassword',viewsController.getforgetpassword)
router.get('/resetPassword',viewsController.getresetPassword)
router.get('/Addtask',authController.protect,viewsController.getAddtask)

router.get('/updatetask',authController.protect,viewsController.getUpdatetask)
router.get('/completeTask',viewsController.getCompletetask)

router.get('/getHistory',viewsController.getHistory)
router.get('/table',viewsController.getTable)
router.get('/about',viewsController.getaboutus)
router.get('/showmore',viewsController.getshowMore)
router.get('/search',viewsController.getsearch)


router
    .route('/OTP')
    .get(viewsController.getOTP)
    .post(userController.enter_OTP)

router
    .route('/reset-password')
    .get(viewsController.getresetPassword)
    .post(userController.reset_password)

module.exports = router