import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
const togglePassword = document.querySelectorAll('.toggle-password');
const passwordInputs = document.querySelectorAll('input[type="password"]');

togglePassword.forEach(function (element, index) {
  element.addEventListener('click', function () {
    const type = passwordInputs[index].getAttribute('type') === 'password' ? 'text' : 'password';
    passwordInputs[index].setAttribute('type', type);
    this.classList.toggle('fa-eye-slash');
    this.classList.toggle('fa-eye');
  });
});
const reset_password= async (password)=>{
    console.log(password)
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/reset-password';
    } else {
        URL = REMOTE_API_URL+'/reset-password';
    }
    try{
        const res = await axios({
            method : 'POST',
            url:URL,
            data:{
                password
            },
        })
        if(res.data.status === 'success'){
            showAlert('success','password Successfully changed')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
            // alert("password Successfully changed")
        }
    }catch(err){
        showAlert('error','Error: sending email')
    }
};
document.querySelector('.form').addEventListener('submit',e=>{
    e.preventDefault();
    const password = document.getElementById('password').value;
    const passwordConfirm = document.getElementById('passwordconfirm').value;
    const passwordRegex = /^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
    const isValid = passwordRegex.test(password);
    
    if(password == passwordConfirm){
        if (isValid) {
            showAlert('error','Password must be at least 8 characters long and contain at least one letter and one number.');
        }else{
            reset_password(password);
        }
    }
    else{
        showAlert('error','password should match')
    }
    console.log(password)
});
