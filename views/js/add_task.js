import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        console.log('This is localhost')
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL
            
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Logout Unsuccessful! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <h4><a href = "/me">My profile</a></h4>
            <h4><a id = "logout">Logout</a></h4>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}


var el1 = document.querySelector('.newTask')
el1.innerHTML =
    `<div class="form-group">
    <label for="description">Title:</label>
    <input type = "text" class="form-control" id="title" placeholder = "One Word Title" required>
    </div>
    <div class="row">
    <div class="col">
        <div class="form-group">
            <label for="start-date">Start Date:</label>
            <input type="date" class="form-control" id="start_date">
        </div> 
    </div>

    <div class="col">
        <div class="form-group">
            <label for="end-date">End Date:</label>
            <input type="date" class="form-control" id="end_date">
        </div>  
    </div>
    </div>

    <div class="form-group">
    <label for="description">Description:</label>
    <textarea type="text" class="form-control" id="description" rows="3" placeholder = "Description" required></textarea>
    </div>

    <div class="form-group">
    <label for="work">Type of work:</label>
    <select class="form-control" id="typeWork">
    <option disabled selected value="" hidden>Type Of Work</option>
    <option>Electrical</option>
    <option>Carpentry</option>
    <option>Mason</option>
    <option>Plumbing</option>
    </select>
    </div>
    <div class="form-group">
    <label for="address">Address</label>
    <input type="text" class="form-control" id="address" placeholder = "Address" required>
    </div> 
    <div class="text-center">
    <button type="submit" class="btn btn-block">Submit</button>
    </div>`

var today = new Date().toISOString().split('T')[0];

// Set the minimum date for the input field
document.getElementById("start_date").min = today;
document.getElementById("end_date").min = today;

export const AddTask = async (title,start_date,end_date,description,type_of_work,address) => {
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks';
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks';
    }
    try {
        // console.log("this is :"+title,start_date,end_date,description,type_of_work,address)
        const res = await axios({
            method: 'POST',
            url:URL,
            data :{
                title,
                start_date,
                end_date,
                description,
                type_of_work,
                address
            }
        })
        if (res.data.status === 'success') {
            showAlert('success', 'new task added!')
            window.setTimeout(() => {
                location.assign('/')
            }, 1500)
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', 'Error: Task not added', message)
    }
}
document.querySelector('.newTask').addEventListener('submit',(e)=>{
    e.preventDefault()
    var title = document.getElementById('title').value
    var start_date = document.getElementById('start_date').value
    var end_date = document.getElementById('end_date').value
    var description = document.getElementById('description').value
    var typeWork = document.getElementById('typeWork').value
    var address = document.getElementById('address').value
    AddTask(title,start_date,end_date,description,typeWork,address)
})

document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}