import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
const check = async (email)=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/forget-password';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/forget-password';
    }
    console.log(email)
    try{
        const res = await axios({
            method : 'POST',
            url:URL,
            data:{
                email
            },
        })
        console.log(res.data.status)
        if(res.data.status === 'success' && res.data.msg === 'MailDone'){
            showAlert('success','Please Check your email')
            window.setTimeout(()=>{
                location.assign('/OTP')
            }, 2000)
        }else{
            showAlert('error','Invalid Email Address')
        }
        
    }catch(err){
        showAlert('error','Error: sending email')
    }
};
document.querySelector('.form').addEventListener('submit',e=>{
    e.preventDefault();
    const email = document.getElementById('email').value;
    check(email);
    // console.log(employeeid,password)
});
