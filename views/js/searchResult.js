import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
const urlParams = new URLSearchParams(window.location.search);
const search = urlParams.get('id');

var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
              location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <a href = "/me"><h4>My profile</h4></a>
            <a id = "logout"><h4>Log out</h4></a>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}

const allTask = async () =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks';
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        // console.log(res.data)
        displayTask(res.data)
    } catch(err){
        console.log(err)
    }
}
allTask()

var card = document.querySelector('.container')
function createCard(title,startdate,category,location,type){
    var div = document.createElement('div')
    div.classList.add("card");

    div.innerHTML = `
                    <img src="`+type+`" class="card-img" alt="" />
                    <div class="card-body">
                    <h2 class="card-sub-title">`+title+`</h2>
                    <h2 class="card-sub-title">`+startdate+`</h2>
                    <h2 class="card-info">`+category+`</h2>
                    <h2 class="card-info1">`+location+`</h2>
                    </div>`
    card.appendChild(div)
}


const displayTask = (news) =>{
    var arr = news.data.task1;
    // console.log(arr)
    // console.log(search)
    var searching = search.toLowerCase()
    for (let i =0; i< arr.length; i++){
        const element = arr[i] 
        // console.log(element)
        var title = element.title.toLowerCase();
        var start_date = element.start_date.toLowerCase();
        var end_date = element.end_date.toLowerCase();
        var description = element.description.toLowerCase();
        var type_of_work = element.type_of_work.toLowerCase();
        var address = element.address.toLowerCase();
        // var budget = element.budget.toLowerCase();
        // var resource = element.resource.toLowerCase();
        if (title.includes(searching)||start_date.includes(searching)||end_date.includes(searching)||description.includes(searching)||type_of_work.includes(searching)||address.includes(searching)) {
            // Create a new result element
            // console.log("database date "+retrievedDate.toDateString()+ "today's date"+today.toDateString())
            var d = new Date(element.start_date)
            var startdate = '' + d.toLocaleString('en-Us',{
                weekday: "long",
                day: "numeric",
                month: "long",
                year: "numeric",
                
            })
            var carpentary = "img/dashboardimg/carpentary.png";
            var electricity = "img/dashboardimg/electricity.png";
            var mason = "img/dashboardimg/mason.png";
            var plumbing ="img/dashboardimg/plumbing.png" ;
            if(element.task_mark_done === 1){
                if (element.type_of_work == "Electrical") {
                    createCard(element.title,startdate,element.type_of_work,element.address,electricity);
                }
                else if(element.type_of_work == "Carpentry"){
                    createCard(element.title,startdate,element.type_of_work,element.address,carpentary);
                }
                else if(element.type_of_work == "Mason"){
                    createCard(element.title,startdate,element.type_of_work,element.address,mason);
                }
                else if(element.type_of_work == "Plumbing"){
                    createCard(element.title,startdate,element.type_of_work,element.address,plumbing);
                }
            }
        }
    }
}
