const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path:'./config.env'})
const app = require('./server')

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD,
)
// const DB  = process.env.DATABASE
const local_DB =  process.env.DATABASE_LOCAL

mongoose.connect(DB).then((con)=>{
    // console.log(con.connections)
    console.log('DB connection successful')
}).catch(error => console.log(error));

const port = 4001

app.listen(port,()=>{
    console.log(`App is running on port ${port} ..`)
})