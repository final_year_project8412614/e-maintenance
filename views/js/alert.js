export const LOCAL_API_URL='http://localhost:4001'
export const REMOTE_API_URL='http://gcit.serv00.net/'

export const hideAlert =()=>{
    const el = document.querySelector('.alert')
    if(el) el.parentElement.removeChild(el)
}

export const showAlert = (type,msg)=>{
    hideAlert()
    const markup = `<div class = "alert alert--${type}">${msg}</div>`
    document.querySelector('body').insertAdjacentHTML('afterbegin',markup)
    window.setTimeout(hideAlert,5000)
}
