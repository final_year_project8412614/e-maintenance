import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
              location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <a href = "/me"><h4>My profile</h4></a>
            <a id = "logout"><h4>Log out</h4></a>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}

const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');
const getTask = async () =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks/'+id;
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks/'+id;
    }
    try{
        console.log(id)
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        displayTask(res.data)
        console.log(res.data)
        // displayTask(res.data)
    } catch(err){
        console.log(err)
    }
}
getTask()

var el1 = document.querySelector('.update_task')
var el2 = document.querySelector('.task_done')
const displayTask = (news) =>{
    var arr = news.data.task1
    var start = new Date(arr.start_date)
    const startdateString = start.toISOString().slice(0, 10);
    
    var end = new Date(arr.end_date)
    const enddateString = end.toISOString().slice(0, 10);

    console.log(startdateString,enddateString)

    el1.innerHTML =
    `<div class="form-group">
        <label for="description">Title:</label>
        <input type = "text" class="form-control" id="title" value = `+arr.title+` required>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="start-date">Start Date:</label>
                <input type="date" class="form-control" id="start_date" value="`+startdateString+`" required>
            </div> 
        </div>

        <div class="col">
            <div class="form-group">
                <label for="end-date">End Date:</label>
                <input type="date" class="form-control" id="end_date" value="`+enddateString+`" required>
            </div>  
        </div>
    </div>

    <div class="form-group">
    <label for="description">Description:</label>
    <input type="text" class="form-control" id="description" value="`+arr.description+`">
    </div>

    <div class="form-group">
    <label for="work">Type of work:</label>
    <select class="form-control" id="typeWork">
        <option selected hidden>`+arr.type_of_work+`</option>
        <option>Electrical</option>
        <option>Carpentry</option>
        <option>Mason</option>
        <option>Plumbing</option>
    </select>
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" value="`+arr.address+`">
    </div> 
    <div class="text-center">
        <button type="submit" class="btn btn-block">Update</button>
    </div>`
    el2.innerHTML = `
                    <div class="col">
                        <div class="form-group">
                            <label for="budget">Budget(In Nu.):</label>
                            <input type="text" class="form-control" style = "height: 5.4rem;"  id="budget" required >
                        </div> 
                    </div>
                    <div class="col">
                        <div class="form-group">
                        <label for="resources">Resources:</label>
                        <textarea type="text" class="form-control" id="resources" required ></textarea>
                        </div>  
                    </div>
                    <div class="col text-center">
                        <button type="submit" class="btn btn-block">Task Done</button>
                    </div>`
    var today = new Date().toISOString().split('T')[0];

    // Set the minimum date for the input field
    document.getElementById("start_date").min = today;
    document.getElementById("end_date").min = today;
}



export const updateTask = async (title,start_date,end_date,description,type_of_work,address) => {
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks/'+id;
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks/'+id;
    }
    try {
        console.log(title,start_date,end_date,description,type_of_work,address)
        const res = await axios({
            method: 'PATCH',
            url:URL,
            data :{
                title,
                start_date,
                end_date,
                description,
                type_of_work,
                address
            }
        })
        if (res.data.status === 'success') {
            showAlert('success', 'Task Successfully updated!')
            window.setTimeout(() => {
                location.assign('/')
            }, 1500)
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', 'Error: Task not added', message)
    }
}

document.querySelector('.update_task').addEventListener('submit',(e)=>{
    e.preventDefault()
    var title = document.getElementById('title').value
    var start_date = document.getElementById('start_date').value
    var end_date = document.getElementById('end_date').value
    var description = document.getElementById('description').value
    var typeWork = document.getElementById('typeWork').value
    var address = document.getElementById('address').value
    // console.log(title,start_date,end_date,description,typeWork,address)
    updateTask(title,start_date,end_date,description,typeWork,address)
})


export const DoneTask = async (budget,resource,task_mark_done) => {
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks/'+id;
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks/'+id;
    }
    try {
        console.log(budget,resource,task_mark_done)
        const res = await axios({
            method: 'PATCH',
            url:URL,
            data :{
                budget,
                resource,
                task_mark_done,
            }
        })
        if (res.data.status === 'success') {
            showAlert('success', 'Task marked Done!')
            window.setTimeout(() => {
                location.assign('/')
            }, 1500)
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', 'Error: Task mark not done', message)
    }
}

document.querySelector('.task_done').addEventListener('submit',(e)=>{
    e.preventDefault()
    var Budget = document.getElementById('budget').value
    var Resources = document.getElementById('resources').value
    // console.log(Budget,Resources)
    DoneTask(Budget,Resources,1)
})

document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}


