const express = require("express")
const path = require('path')
const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const taskRouter = require('./routes/taskRoutes')
const cookieparser = require('cookie-parser')
app.use(cookieparser())

app.use(express.json())
app.use('/api/v1/users', userRouter)
app.use('/api/v1/tasks',taskRouter)
app.use('/',viewRouter)

app.use(express.static(path.join(__dirname,'views')))

module.exports = app


