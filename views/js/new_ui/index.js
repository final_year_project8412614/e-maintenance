import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from '../alert.js'
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const togglePassword = document.querySelectorAll('.toggle-password');
const passwordInputs = document.querySelectorAll('input[type="password"]');

togglePassword.forEach(function (element, index) {
  element.addEventListener('click', function () {
    const type = passwordInputs[index].getAttribute('type') === 'password' ? 'text' : 'password';
    passwordInputs[index].setAttribute('type', type);
    this.classList.toggle('fa-eye-slash');
    this.classList.toggle('fa-eye');
  });
});
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        console.log('This is localhost')
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('Logging out unsuccessful! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')
var addTaskbutton = document.querySelector('.AddTaskbutton')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <h4><a href = "/me">My profile</a></h4>
            <h4><a id = "logout">Logout</a></h4>
        </div>
    `
    el.innerHTML=``
    addTaskbutton.innerHTML = `<a href="/Addtask" class="btn">Add Task</a>
                                <a href="/getHistory" class="btn-btn">Task Completed</a>`
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{

    addTaskbutton.innerHTML = '<a href="/getHistory" class="btn">Task Completed</a>'
    el.innerHTML = `<button class="btn-btn">Login</button>`

}

const login = async (employee_id,password)=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/login';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/login';
    }
    console.log(employee_id,password)
    try{
        const res = await axios({
            method : 'POST',
            url:URL,
            data:{
                employee_id,
                password,
            },
        })
        if(res.data.status === 'success'){
            showAlert('success','Logged in successfully')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
            var obj = res.data.data.user
            console.log(obj)
            document.cookie = 'token = '+JSON.stringify(obj)
            console.log(obj)
        }
    }catch(err){
        let message = 
            typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        showAlert('error','Incorrect Employee ID or Password',message)
    }
};
document.querySelector('.login-form').addEventListener('submit',e=>{
    e.preventDefault();
    const employeeid = document.getElementById('employeeid').value;
    const password  = document.getElementById('password').value
    login(employeeid,password);
    // console.log(employeeid,password)
});


var task = document.querySelector('.swiper-wrapper')

function createDiv(title,address,id,image){
        var div = document.createElement('div')
        div.classList.add("swiper-slide","slide");
        var text =` <div class="image">
                            <img src="`+image+`" alt="" />
                        </div>
                        <div class="content">
                            <h3>`+title+`</h3>
                            <p>`+address+`</p>`
        if(obj._id){
            text +=`<a href="/updatetask?id=${id}" class="btn">Update Task</a>`   
            
        }else{
            text +=`</div>`
        }                                  
        div.innerHTML = text
        task.appendChild(div)
}
                        // <div class="card">
                        //     <div class="img-wrapper">
                        //         <div class="img-wrapper"><img src="`+image+`" class="d-block w-100" alt="..."> </div>
                        //     </div>
                        //     <div class="card-body">
                        //         <h5 class="card-title">`+title+`</h5>
                        //         <p class="card-text">`+address+`</p>
                        //         <a href="/updatetask?id=${id}" class="btn-update">Update Task</a>
                        //     </div>
                        // </div>



const allTask = async () =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks';
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        displayComplete(res.data)
        displayTaskPercent(res.data)
        displayTask(res.data)
    } catch(err){
        console.log(err)
    }
}
allTask()

var carpentaryshow = document.getElementById("carpentry")
var electricityshow = document.getElementById("Electricity")
var masonshow = document.getElementById("mason")
var plumbingshow = document.getElementById("plumbing")
const ctx = document.getElementById('barchart');


const displayTaskPercent = (news)=>{
    var arr = news.data.task1
    var carpentary = 0;
    var electricity =0;
    var mason=0;
    var plumbing=0;
    for (let i =0; i< arr.length; i++){
        const element = arr[i]
        // console.log(element.task_mark_done)
        if(element.task_mark_done === 1){
            if (element.type_of_work == "Electrical") {
                electricity =electricity+1
            }
            else if(element.type_of_work == "Carpentry"){
                carpentary =carpentary+1;
            }
            else if(element.type_of_work == "Mason"){
                mason =mason+1;
            }
            else if(element.type_of_work == "Plumbing"){
                plumbing = plumbing+1 
            }
        }
    }
    // console.log("carpentary "+carpentary)
    // console.log("electricity "+electricity)
    // console.log("mason "+mason)
    // console.log("plumbing "+plumbing)

    var total = carpentary+electricity+mason+plumbing
    // console.log(total)
    carpentaryshow.innerHTML = ``+((carpentary/total)*100).toFixed(1)+`%`
    electricityshow.innerHTML = ``+((electricity/total)*100).toFixed(1)+`%`
    masonshow.innerHTML = ``+((mason/total)*100).toFixed(1)+`%`
    plumbingshow.innerHTML = ``+((plumbing/total)*100).toFixed(1)+`%`
    // console.log(carpentary,electricity,mason,plumbing)
    // graph

    new Chart(ctx, {
        type: 'bar',
        data: {
        labels: ['Carpentry', 'Plumbing', 'Mason', 'Electrical'],
        datasets: [
            {
            label: 'Types of work done',
            data: [carpentary, plumbing, mason, electricity],
            borderWidth: 1,
            },
        ],
        },
        options: {
        scales: {
            y: {
            beginAtZero: true,
            },
        },
        },
    });
}

const ctx2 = document.getElementById('doughnut');
const displayComplete = (news) =>{
    var arr = news.data.task1
    var todo = 0;
    var completed = 0;
    // console.log(arr)
    for (let i =0; i< arr.length; i++){
        const element = arr[i]
        if(element.task_mark_done === 0){
            todo = todo+1;
        }else{
            completed = completed+1;
        }
    }
    new Chart(ctx2, {
        type: 'doughnut',
        data: {
          labels: ['Todo', 'completed Task'],
          datasets: [
            {
              data: [todo, completed],
              borderWidth: 1,
            },
          ],
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        },
      });
}

const displayTask = (news) =>{
    var arr = news.data.task1
    // console.log(arr)
    for (let i =0; i< arr.length; i++){
        const element = arr[i]
        const retrievedDate = new Date(element.start_date);
        // Get today's date
        const today = new Date();

        // console.log("database date "+retrievedDate.toDateString()+ "today's date"+today.toDateString())
        if(element.task_mark_done === 0 && retrievedDate.toDateString()===today.toDateString() ){
            if (element.type_of_work == "Electrical") {
                createDiv(element.title,element.address,element._id,"img/dashboardimg/electricity.png")
            }
            else if(element.type_of_work == "Carpentry"){
                createDiv(element.title,element.address,element._id,"img/dashboardimg/carpentary.png")
            }
            else if(element.type_of_work == "Mason"){
                createDiv(element.title,element.address,element._id,"img/dashboardimg/mason.png")
            }
            else if(element.type_of_work == "Plumbing"){
                createDiv(element.title,element.address,element._id,"img/dashboardimg/plumbing.png")
            }
                // console.log(element.title,element.address,element._id)
        }
    }
}

document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}


