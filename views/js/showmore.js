import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
              location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <h4><a href = "/me">My profile</a></h4>
            <h4><a id = "logout">Logout</a></h4>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}
var tomorrowtask = document.getElementById('tomorrowTask')
function createDivtomorrow(title,address,id,image){
        console.log(title,address,id)
        var div = document.createElement('div')
        div.classList.add("swiper-slide","slide");
        var text =` <div class="image">
                            <img src="`+image+`" alt="" />
                        </div>
                        <div class="content">
                            <h3>`+title+`</h3>
                            <p>`+address+`</p>`
        if(obj._id){
            text +=`<a href="/updatetask?id=${id}" class="btn">Update Task</a>`   
            
        }else{
            text +=`</div>`
        }                                  
        div.innerHTML = text
        tomorrowtask.appendChild(div)
}
var dayAftertomorrowtask = document.getElementById('AftertomorrowTask')
function createDivAftertomorrow(title,address,id,image){
        var div = document.createElement('div')
        div.classList.add("swiper-slide","slide");
        var text =` <div class="image">
                            <img src="`+image+`" alt="" />
                        </div>
                        <div class="content">
                            <h3>`+title+`</h3>
                            <p>`+address+`</p>`
        if(obj._id){
            text +=`<a href="/updatetask?id=${id}" class="btn">Update Task</a>`   
            
        }else{
            text +=`</div>`
        }                                  
        div.innerHTML = text
        dayAftertomorrowtask.appendChild(div)
}
var othertodo = document.getElementById('otherTodo')
function otherTodo(title,address,id,image){
        var div = document.createElement('div')
        div.classList.add("swiper-slide","slide");
        var text =` <div class="image">
                            <img src="`+image+`" alt="" />
                        </div>
                        <div class="content">
                            <h3>`+title+`</h3>
                            <p>`+address+`</p>`
        if(obj._id){
            text +=`<a href="/updatetask?id=${id}" class="btn">Update Task</a>`   
            
        }else{
            text +=`</div>`
        }                                  
        div.innerHTML = text
        othertodo.appendChild(div)
}



const allTask = async () =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks';
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        otherworks(res.data)
        displayTask(res.data)
    } catch(err){
        console.log(err)
    }
}
allTask()

const otherworks = (news) =>{
    var arr = news.data.task1
    for (let i =0; i< arr.length; i++){
        const element = arr[i]
        if(element.task_mark_done === 0){
            if (element.type_of_work == "Electrical") {
                console.log(element.title,element.address,element._id,"img/dashboardimg/electricity.png")
                otherTodo(element.title,element.address,element._id,"img/dashboardimg/electricity.png")
            }
            else if(element.type_of_work == "Carpentry"){
                otherTodo(element.title,element.address,element._id,"img/dashboardimg/carpentary.png")
            }
            else if(element.type_of_work == "Mason"){
                otherTodo(element.title,element.address,element._id,"img/dashboardimg/mason.png")
            }
            else if(element.type_of_work == "Plumbing"){
                otherTodo(element.title,element.address,element._id,"img/dashboardimg/plumbing.png")
            }

        }
    }
}


const displayTask = (news) =>{
    var arr = news.data.task1
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(today.getDate() + 1);
    const dayaftertomorrow = new Date(today)
    dayaftertomorrow.setDate(today.getDate() + 2);
    // console.log(tomorrow)
    // console.log(dayaftertomorrow)
    for (let i =0; i< arr.length; i++){
        const element = arr[i]
        const retrievedDate = new Date(element.start_date);
        // Get today's date
        // console.log("database date "+retrievedDate.toDateString()+ "today's date"+tomorrow.toDateString())
        if(element.task_mark_done === 0 && retrievedDate.toDateString()===tomorrow.toDateString() ){
            if (element.type_of_work == "Electrical") {
                createDivtomorrow(element.title,element.address,element._id,"img/dashboardimg/electricity.png")
            }
            else if(element.type_of_work == "Carpentry"){
                createDivtomorrow(element.title,element.address,element._id,"img/dashboardimg/carpentary.png")
            }
            else if(element.type_of_work == "Mason"){
                createDivtomorrow(element.title,element.address,element._id,"img/dashboardimg/mason.png")
            }
            else if(element.type_of_work == "Plumbing"){
                createDivtomorrow(element.title,element.address,element._id,"img/dashboardimg/plumbing.png")
            }
                // console.log(element.title,element.address,element._id)
        }
        else if(element.task_mark_done === 0 && retrievedDate.toDateString()===dayaftertomorrow.toDateString() ){
            if (element.type_of_work == "Electrical") {
                createDivAftertomorrow(element.title,element.address,element._id,"img/dashboardimg/electricity.png")
            }
            else if(element.type_of_work == "Carpentry"){
                createDivAftertomorrow(element.title,element.address,element._id,"img/dashboardimg/carpentary.png")
            }
            else if(element.type_of_work == "Mason"){
                createDivAftertomorrow(element.title,element.address,element._id,"img/dashboardimg/mason.png")
            }
            else if(element.type_of_work == "Plumbing"){
                createDivAftertomorrow(element.title,element.address,element._id,"img/dashboardimg/plumbing.png")
            }
                // console.log(element.title,element.address,element._id)
        }
    }
}

document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}

