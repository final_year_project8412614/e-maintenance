import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from '../alert.js'
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')
var addTaskbutton = document.querySelector('.AddTaskbutton')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <h4><a href = "/me">My profile</a></h4>
            <h4><a id = "logout">Logout</a></h4>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}

const login = async (employee_id,password)=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/login';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/login';
    }
    console.log(employee_id,password)
    try{
        const res = await axios({
            method : 'POST',
            url:URL,
            data:{
                employee_id,
                password,
            },
        })
        if(res.data.status === 'success'){
            showAlert('success','Logged in successfully')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
            var obj = res.data.data.user
            console.log(obj)
            document.cookie = 'token = '+JSON.stringify(obj)
            console.log(obj)
        }
    }catch(err){
        let message = 
            typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        showAlert('error','Error: Incorrect employeeid or password',message)
    }
};
document.querySelector('.login-form').addEventListener('submit',e=>{
    e.preventDefault();
    const employeeid = document.getElementById('employeeid').value;
    const password  = document.getElementById('password').value
    login(employeeid,password);
    // console.log(employeeid,password)
});
