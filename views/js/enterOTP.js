import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'

const check = async (otp)=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/OTP';
    } else {
        URL = REMOTE_API_URL+'/OTP';
    }
    console.log(otp)
    try{
        const res = await axios({
            method : 'POST',
            // url: REMOTE_API_URL+'/OTP',
            url:URL,
            data:{
                otp
            },
        })
        // console.log(res.data.status)
        if(res.data.status === 'success'){
            showAlert('success','Correct OTP')
            window.setTimeout(()=>{
                location.assign('/resetPassword')
            }, 2000)
        }
    }catch(err){
        showAlert('error','Error: Please check the OTP')
    }
};
document.querySelector('.form').addEventListener('submit',e=>{
    e.preventDefault();
    const otp = document.getElementById('otp').value;
    check(otp);
    // console.log(employeeid,password)
});
