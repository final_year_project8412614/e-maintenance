const path =  require('path')

// exports.getLoginForm = (req,res) =>{
//     res.sendFile(path.join(__dirname,'../','views','login.html'))
// }

exports.getSignupForm = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','signup.html'))
}

exports.getHome = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','index.html'))
}
exports.getprofile = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','myprofilepage.html'))
}

exports.getforgetpassword = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','forgetpassword.html'))
}
exports.getresetPassword = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','resetPassword.html'))
}
exports.getAddtask = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','add_task.html'))
}
exports.getUpdatetask = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','update_task.html'))
}
exports.getCompletetask = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','task_complete.html'))
}
exports.getOTP = (req,res) =>{
    res.sendFile(path.join(__dirname,'../','views','enterOTP.html'))
}
exports.getHistory = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','history.html'))
}
exports.getTable = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','table_data.html'))
}
exports.getaboutus = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','Aboutus.html'))
}

exports.getshowMore = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','showmore.html'))
}

exports.getsearch = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','searchResult.html'))
}