import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
const urlParams = new URLSearchParams(window.location.search);
const Year_id = urlParams.get('id');
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
              location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')
var yearDisplay = document.getElementById('year_display')
yearDisplay.innerHTML = `This is the data for `+Year_id


if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <a href = "/me"><h4>My profile</h4></a>
            <a id = "logout"><h4>Log out</h4></a>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{

    el.innerHTML = `<button class="btn-btn">Login</button>`

}

const login = async (employee_id,password)=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/login';
    } else {
        URL = 'https://estatemaintainancemanagementsystem.onrender.com/api/v1/users/login';
    }
    console.log(employee_id,password)
    try{
        const res = await axios({
            method : 'POST',
            url:URL,
            data:{
                employee_id,
                password,
            },
        })
        if(res.data.status === 'success'){
            showAlert('success','Logged in successfully')
            window.setTimeout(()=>{
                location.assign('/')
            }, 1500)
            var obj = res.data.data.user
            console.log(obj)
            document.cookie = 'token = '+JSON.stringify(obj)
            console.log(obj)
        }
    }catch(err){
        let message = 
            typeof err.response !== 'undefined'
            ? err.response.data.message
            : err.message
        showAlert('error','Error: Incorrect employeeid or password',message)
    }
};
document.querySelector('.login-form').addEventListener('submit',e=>{
    e.preventDefault();
    const employeeid = document.getElementById('employeeid').value;
    const password  = document.getElementById('password').value
    login(employeeid,password);
    // console.log(employeeid,password)
});

var table = document.querySelector('.table')
function createtableRow(slno,title,start,end,description,type_of_work,address,task_mark_done,budget,resource){
    var tr = document.createElement('tr')
    tr.innerHTML = `   <td data-label="Slno. : ">`+slno+`</td>
                        <td data-label="Title : ">`+title+`</td>
                        <td data-label="Start Date : ">`+start+`</td>
                        <td data-label="End Date : ">`+end+`</td>
                        <td data-label="Description : ">`+description+`</td>
                        <td data-label="Type of work : ">`+type_of_work+`</td>
                        <td data-label="Address : ">`+address+`</td>
                        <td data-label="Progress : ">`+task_mark_done+`</td>
                        <td data-label="Budget : ">`+budget+`</td>
                        <td data-label="Resources : ">`+resource+`</td>`
    table.appendChild(tr)
}


const allTask = async () =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks/yearly';
    } else {
        URL = 'https://estatemaintainancemanagementsystem.onrender.com/api/v1/tasks/yearly';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        // console.log(res.data)
        displayTask(res.data)
    } catch(err){
        console.log(err)
    }
}
allTask()
var slno = 0;
var total = document.getElementById('totalBudget')

const displayTask = (news) =>{
    var arr = news.data.groupedPosts
    const Years = Object.keys(arr)
    // console.log(Years)
    var total_Budget = 0;
    if (Years){
        for (let year in Years) {
            if(Year_id === Years[year]){
                const postsForYear = arr[Years[year]];
                console.log(postsForYear) 

                for (let i =0; i< postsForYear.length; i++){
                    const element = postsForYear[i]
                    // console.log("This is element "+element)

                    if(element.task_mark_done === 1){
                        slno = slno+1;
                        total_Budget = total_Budget+Number(element.budget);
                        const start = new Date(element.start_date);
                        const end = new Date(element.end_date);
                        const start_format = start.toDateString()
                        const end_format = end.toDateString()
                        // console.log(slno,element.title,start_format,end_format,element.description,element.type_of_work,element.address,element.task_mark_done,element.budget,element.resource)
                        createtableRow(slno,element.title,start_format,end_format,element.description,element.type_of_work,element.address,'Done',element.budget,element.resource)
                    }
                }
            }
        }
    }
    total.innerHTML = `Total Budget : `+total_Budget
}



// var slno = 0;
// const displayTableData = (news) =>{
//     var arr = news.data.task1
//     // console.log(arr)
//     for (let i =0; i< arr.length; i++){
//         const element = arr[i]
//         if(element.task_mark_done === 1){
//             slno = slno+1;
//             const start = new Date(element.start_date);
//             const end = new Date(element.end_date);
//             const start_format = start.toDateString()
//             const end_format = end.toDateString()
//             // console.log(slno,element.title,start_format,end_format,element.description,element.type_of_work,element.address,element.task_mark_done,element.budget,element.resource)
//             createtableRow(slno,element.title,start_format,end_format,element.description,element.type_of_work,element.address,element.task_mark_done,element.budget,element.resource)
//         }
//     }
// }
document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}