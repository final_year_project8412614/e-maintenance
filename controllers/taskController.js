const task = require('./../models/taskModels')
const moment = require('moment');
 
exports.getAllTask = async (req, res) => {
  try {
    const task1 = await task.find()
    res.status(200).json({
      status: 'success',
      results: task1.length,
      data: {
        task1,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: 'Invalid data sent!',
    })
  }
}
exports.getAllTaskYearly = async (req, res) => {
  try {
    const posts = await task.find()
    // console.log(posts)
    
    // Group posts by year
    const groupedPosts = {};
    posts.forEach((post) => {
      const year = moment(post.start_date).subtract(6, 'months').format('YYYY');
      const nextYear = moment(post.start_date).add(6, 'months').format('YYYY');
      const key = `${year}-${nextYear}`;
      if (!groupedPosts[key]) {
        groupedPosts[key] = [];
      }
      groupedPosts[key].push(post);
    });

    // console.log(groupedPosts);
    res.status(200).json({
      status: 'success',
      results: groupedPosts.length,
      data: {
        groupedPosts,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: 'Invalid data sent!',
    })
  }
}
 
exports.getTask = async (req, res) => {
  try {
    const task1 = await task.findById(req.params.id)
    res.status(200).json({
      status: 'success',
      results: task1.length,
      data: {
        task1,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err,
    })
  }
}
 
exports.createTask = async (req, res) => {
  try {
    const newTask = await task.create(req.body)
    console.log(newTask)
 
    res.status(201).json({
      status: 'success',
      data: {
        task: newTask,
      },
    })
  } catch (err) {
    res.status(400).json({
      status: 'fail',
      message: err,
    })
  }
}
 
exports.updateTask = async (req, res) => {
  try {
    const task1 = await task.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    })
 
    res.status(200).json({
      status: 'success',
      data: {
        task1,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err,
    })
  }
}
 
exports.deleteTask = async (req, res) => {
  try {
    const task1 = await task.findByIdAndDelete(req.params.id)
 
    res.status(200).json({
      status: 'success',
      data: {
        task1,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err,
    })
  }
}
