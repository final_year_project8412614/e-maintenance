const mongoose = require('mongoose')
const validator = require ('validator')
const bcrypt = require('bcryptjs')
const userSchema = new mongoose. Schema({
    employee_id:{
        type:String,
        required:[true,'please enter your employee ID']
    },
    name: {
        type: String, 
        required:[ true,'Please tell us your name!'],
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true, 
        lowercase: true, 
        validate: [validator.isEmail ,'Please provide a valid email'],
    },
    photo:{
        type: String,
        default:'default.jpg',
    },
    password:{
        type: String, 
        required: [true,'Please provide a password!'],
        minlength: 8,
        //password wont be included when we get the users select: false,
        select:false,
    },
    passwordConfirm:{
        type: String,
        required:[true,'Please confirm your password'],
        validate:{
            validator:function(el){
                return el === this.password
            },
            message: 'Passwords are not the same',
        },
    },
    token:{
        type: String,
        default:''
    }
})

userSchema.pre('save',async function(next){
    if (!this.isModified('password')) return next()
    this.password = await bcrypt.hash(this.password,12)
    this.passwordConfirm = undefined
    next()
})
userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword,userPassword)
}


const User = mongoose.model('User',userSchema)
module.exports = User