const mongoose = require('mongoose')
 
const taskSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  start_date:{
    type : Date,
  },
  end_date: {
    type: Date,
  },
  description: {
    type: String,
  },
  type_of_work:{
    type: String, 
  },
  address: {
    type: String,
  },
  task_mark_done:{
    type: Number,
    default:0,
  },
  budget: {
    type: String,
    default:'',
  },
  resource: {
    type: String,
    default:'',
  },
})
 
const newsFeed = mongoose.model('task_list', taskSchema)
module.exports = newsFeed

