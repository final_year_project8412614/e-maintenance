import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
const urlParams = new URLSearchParams(window.location.search);
const Year_id = urlParams.get('id');

var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/users/logout';
    } else {
        URL = REMOTE_API_URL+'/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method:'GET',
            url:URL,                                                      
        })
        if(res.data.status === 'success'){
            showAlert('success', 'Successfully logged out')
            window.setTimeout(()=>{
              location.assign('/')
            }, 1500)
        }
    }catch(err){
        showAlert('error', 'Error logging out! Try again.')
    }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')
var table = document.querySelector('.tabledata')

table.innerHTML = `<center><a href="/table?id=${Year_id}"><button class = "btn-btn" style = "background-color: aliceblue;"> view in table</button></a></center>`

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <a href = "/me"><h4>My profile</h4></a>
            <a id = "logout"><h4>Log out</h4></a>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}

var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}

const allTask = async () =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = LOCAL_API_URL+'/api/v1/tasks/yearly';
    } else {
        URL = REMOTE_API_URL+'/api/v1/tasks/yearly';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        // console.log(res.data)
        displayTask(res.data)
    } catch(err){
        console.log(err)
    }
}
allTask()

var card = document.querySelector('.container')
function createCard(title,startdate,category,location,type){
    var div = document.createElement('div')
    div.classList.add("card");

    div.innerHTML = `
                    <img src="`+type+`" class="card-img" alt="" />
                    <div class="card-body">
                    <h2 class="card-sub-title">`+title+`</h2>
                    <h2 class="card-sub-title">`+startdate+`</h2>
                    <h2 class="card-info">`+category+`</h2>
                    <h2 class="card-info1">`+location+`</h2>
                    </div>`
    card.appendChild(div)
}


const displayTask = (news) =>{
    var arr = news.data.groupedPosts
    const Years = Object.keys(arr)
    // console.log(Years)
    if (Years){
        for (let year in Years) {
            if(Year_id === Years[year]){
                const postsForYear = arr[Years[year]];
                console.log(postsForYear) 

                for (let i =0; i< postsForYear.length; i++){
                const element = postsForYear[i]
                console.log("This is element "+element)

                if(element.task_mark_done === 1){
                    var title = element.title
                    var d = new Date(element.start_date)
                    var startdate = '' + d.toLocaleString('en-Us',{
                        weekday: "long",
                        day: "numeric",
                        month: "long",
                        year: "numeric",
                        
                    })
                    var category = element.type_of_work;
                    var location = element.address;
                    var carpentary = "img/dashboardimg/carpentary.png";
                    var electricity = "img/dashboardimg/electricity.png";
                    var mason = "img/dashboardimg/mason.png";
                    var plumbing ="img/dashboardimg/plumbing.png" ;

                    if (element.type_of_work == "Electrical") {
                        createCard(title,startdate,category,location,electricity);
                    }
                    else if(element.type_of_work == "Carpentry"){
                        createCard(title,startdate,category,location,carpentary);
                    }
                    else if(element.type_of_work == "Mason"){
                        createCard(title,startdate,category,location,mason);

                    }
                    else if(element.type_of_work == "Plumbing"){
                        createCard(title,startdate,category,location,plumbing);
                    }

                }
            }
            }
        }
    }
}
document.querySelector('.search-form').addEventListener('submit',e=>{
    e.preventDefault();
    const search = document.getElementById('search-box').value;
    searchname(search);
    // console.log(employeeid,password)
});
function searchname(search){
    window.setTimeout(()=>{
        location.assign(`/search?id=${search}`)
    }, 500)
}