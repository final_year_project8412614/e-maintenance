import { showAlert , REMOTE_API_URL, LOCAL_API_URL } from './alert.js'
var obj
if(document.cookie){
    obj = JSON.parse(document.cookie.substring(6))
}else{
    obj = JSON.parse('{}')
}
const logout = async()=>{
  let URL;
  if (window.location.hostname === 'localhost') {
      URL = LOCAL_API_URL+'/api/v1/users/logout';
  } else {
      URL = REMOTE_API_URL+'/api/v1/users/logout';
  }
  try{
      const res = await axios({
          method:'GET',
          url:URL,
      })
      if(res.data.status === 'success'){
          showAlert('success', 'Successfully logged out')
          window.setTimeout(()=>{
            location.assign('/')
          }, 1500)
      }
  }catch(err){
      showAlert('error', 'Error logging out! Try again.')
  }
}
var el = document.querySelector('.login_button')
var user = document.querySelector('.user')

if(obj._id){
    user.innerHTML=
    `   <div class="action">
            <div class="profile" onclick="profile();">
                <img src="../img/users/`+obj.photo+`" alt="" width="47px">
            </div>
        <div class="menu">
            <h3>`+obj.name+`</h3>
            <h4><a href = "/me">My profile</a></h4>
            <h4><a id = "logout">Logout</a></h4>
        </div>
    `
    el.innerHTML=``
    var doc = document.querySelector('#logout')

    doc.addEventListener('click',(e)=> logout())

}else{
    el.innerHTML = `<button class="btn-btn">Login</button>`

}


var el1 = document.querySelector('.user_img')
el1.innerHTML = `<div class="img-circle text-center mb-3">
                    <img src="../img/users/` +obj.photo +`" alt="Image" class="shadow">
                  </div>
                  <h4 class="text-center">`+obj.name.toUpperCase() +`</h4>`

var el2 = document.querySelector('.update_User')
el2.innerHTML = ` <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" id="name" class="form-control" value="` +obj.name.toUpperCase() +`">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input id="email" type="email" class="form-control" value="`+obj.email +`">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label>Employee ID</label>
                        <input id="employee_id" type="text" class="form-control" value="`+obj.employee_id+`">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label>Change Profile</label>
                        <input type="file" accept="image/*" id="photo" name="photo" class="form-control" value="`+obj.employee_id+`">
                    </div>
                  </div>
                  </div>
                  <div>
                  <button class="btn btn-primary" type="submit">Update</button>
                  </div>`
var el3 = document.querySelector('.form-user-password')
el3.innerHTML = `
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Old password</label>
                    <div class="password-wrapper">
                      <input type="password" id="password-current" class="form-control" required minlength="8">
                      <i class="toggle-password fas fa-eye-slash"></i>
                    </div>
                  </div>
                </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>New password</label>
                      <div class="password-wrapper">
                        <input type="password" id="password-new" required minlength="8" class="form-control">
                        <i class="toggle-password fas fa-eye-slash"></i>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Confirm new password</label>
                      <div class="password-wrapper">
                        <input type="password" id="password-confirm" required minlength="8" class="form-control">
                        <i class="toggle-password fas fa-eye-slash"></i>
                      </div>
                    </div>
                  </div>
                </div>

                <div>
                <button class="btn btn-primary btn--save-password">Save password</button>
                </div>
            `
const togglePassword = document.querySelectorAll('.toggle-password');
const passwordInputs = document.querySelectorAll('input[type="password"]');

togglePassword.forEach(function (element, index) {
  element.addEventListener('click', function () {
    const passwordInput = this.previousElementSibling;
    const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordInput.setAttribute('type', type);
    this.classList.toggle('fa-eye-slash');
    this.classList.toggle('fa-eye');
  });
});            

 
//Updating settings

 
// type is either 'password' or data
export const updateSettings = async (data, type) => {
  try {
    let url;
    if (window.location.hostname === 'localhost') {
      url =
        type === 'password'
          ? LOCAL_API_URL+'/api/v1/users/updateMyPassword'
          : LOCAL_API_URL+'/api/v1/users/updateMe';
    } else {
      url =
        type === 'password'
          ? REMOTE_API_URL+'/api/v1/users/updateMyPassword'
          : REMOTE_API_URL+'/api/v1/users/updateMe';
    }
    const res = await axios({
      method: 'PATCH',
      url,
      data,
    })
    console.log(res.data.status)
    if (res.data.status === 'success') {
      showAlert('success', 'Data updated successfully!')
    }
  } catch (err) {
    let message =
      typeof err.response !== 'undefined'
        ? err.response.data.message
        : err.message
    // showAlert('error', 'Error: Please provide valid email address', message)
    showAlert('error', err.response.data.message)
  }
}
 
  const userDataForm = document.querySelector('.update_User')
  userDataForm.addEventListener('submit', (e) => {
    e.preventDefault()
    var obj = JSON.parse(document.cookie.substring(6))
    const form = new FormData()
    form.append('name', document.getElementById('name').value)
    form.append('email', document.getElementById('email').value)
    form.append('employee_id', document.getElementById('employee_id').value)
    form.append('photo', document.getElementById('photo').files[0])
    form.append('userId', obj._id)
    console.log(form)
    updateSettings(form, 'data')
  })
 
  const userPasswordForm = document.querySelector('.form-user-password')
  userPasswordForm.addEventListener('submit', async (e) => {
    e.preventDefault()
 
    document.querySelector('.btn--save-password').textContent = 'Updating...'
    const passwordCurrent = document.getElementById('password-current').value
    const password = document.getElementById('password-new').value
    const passwordConfirm = document.getElementById('password-confirm').value
    // console.log(passwordCurrent,password,passwordConfirm)
    
// Password validation
    const passwordRegex = /^(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
    const isValid = passwordRegex.test(password);
    if (isValid) {
      showAlert('error','Password must be at least 8 characters long and contain at least one letter and one number.');
    }else{
      await updateSettings(
        { passwordCurrent, password, passwordConfirm },
        'password',
      )
    }
    document.querySelector('.btn--save-password').textContent = 'Save password'
    document.getElementById('password-current').value = ''
    document.getElementById('password-new').value = ''
    document.getElementById('password-confirm').value = ''
  })

  // header
let navbar = document.querySelector('.header .navbar');
let searchForm = document.querySelector('.header .search-form');
let loginForm = document.querySelector('.header .login-form');
let contactInfo = document.querySelector('.contact-info');

document.querySelector('#menu-btn').onclick = () => {
  navbar.classList.toggle('active');
  searchForm.classList.remove('active');
  loginForm.classList.remove('active');
};

document.querySelector('#search-btn').onclick = () => {
  searchForm.classList.toggle('active');
  navbar.classList.remove('active');
  loginForm.classList.remove('active');
};

document.querySelector('#login-btn').onclick = () => {
  loginForm.classList.toggle('active');
  navbar.classList.remove('active');
  searchForm.classList.remove('active');
};

document.querySelector('#info-btn').onclick = () => {
  contactInfo.classList.add('active');
};

document.querySelector('#close-contact-info').onclick = () => {
  contactInfo.classList.remove('active');
};

window.onscroll = () => {
  navbar.classList.remove('active');
  searchForm.classList.remove('active');
  loginForm.classList.remove('active');
  contactInfo.classList.remove('active');
};

document.querySelector('.search-form').addEventListener('submit',e=>{
  e.preventDefault();
  const search = document.getElementById('search-box').value;
  searchname(search);
  // console.log(employeeid,password)
});
function searchname(search){
  window.setTimeout(()=>{
      location.assign(`/search?id=${search}`)
  }, 500)
}